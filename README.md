# boilerplates and standards for piperci

## Standard faas function
Use the noop-faas function as a basis for all faas functions.

[piperci-noop-faas](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas)

## Standard toolsets

- bash
- bindep
- coverage
- docker
- docker-swarm
- flake8
  ```
    flake8>=3.6.0,<4
    flake8-bandit
    flake8-debugger
    ; flake8-isort
    flake8-eradicate
    flake8-mutable
  ```
- gitlab-ci
- isort
- pytest
- python3.7.3
- tox
- uwsgi

## Standard licence
  MIT License


## Python code standards

In addition to passing the flake8 lint stage based on the config in tox.ini and plugins listed above.

Python coding practices:
 - Code is to run in Python 3.7.3
 - New code should use type hinting for external interfaces intended to be used by other developers. See: [PEP 484](https://www.python.org/dev/peps/pep-0484/)
 - Merge requests must pass the standard linting.
 - DRY principles unless it doesn't make sense.
 - Use Pytest. Initial coverage should be greater then `80%`
 - All code must be contained in a valid module with an `__init__.py`
 - Modules should make use of exceptions, functions should not introduce side effects... like `print()` or `sys.exit()`
 - Logging and debug should use the python standard `logging` library
